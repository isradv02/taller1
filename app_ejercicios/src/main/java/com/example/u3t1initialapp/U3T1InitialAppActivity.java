package com.example.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class U3T1InitialAppActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;
    private TextView tvDisplay;
    //TODO Ex1 increase and decrease by 2
    private Button buttonIncrease, buttonDecrease,buttonIncrease2, buttonDecrease2, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        //TODO Ex1 increase and decrease by 2
        buttonIncrease2 = findViewById(R.id.buttonIncrease2);
        //TODO Ex1 increase and decrease by 2
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        //TODO Ex1 increase and decrease by 2
        buttonIncrease2.setOnClickListener(this);
        //TODO Ex1 increase and decrease by 2
        buttonDecrease2.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.buttonIncrease:count++; break;
            case R.id.buttonDecrease:count--; break;
            //TODO Ex1 increase and decrease by 2
            case R.id.buttonIncrease2:count+=2; break;
            //TODO Ex1 increase and decrease by 2
            case R.id.buttonDecrease2:count-=2; break;
            case R.id.buttonReset:count = 0; break;
        }
        tvDisplay.setText(getString(R.string.number_of_elements)+": "+count);
    }

    //TODO Ex3 save and restore UI
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("contador", count);
        super.onSaveInstanceState(outState);
    }
    //TODO Ex3 save and restore UI
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        count = savedInstanceState.getInt("contador");
        tvDisplay.setText(getString(R.string.number_of_elements)+": "+count);
        super.onRestoreInstanceState(savedInstanceState);
    }
}